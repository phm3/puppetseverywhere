exec {'apt-update':
	path    => '/usr/bin',
					command => 'apt-get update'
}

class {'git':}
class {'apache':}
class {'php':}
class {'addons::nginx':}
