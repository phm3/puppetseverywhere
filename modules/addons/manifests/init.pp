class addons::nginx {

file { '/home/vagrant/test.sh': 
		ensure => present,
					 mode => 0777,
					 source => [
						 "puppet:///modules/addons/www_folder.sh",
					 ],
	}

	exec { '/home/vagrant/test.sh': require => File['/home/vagrant/test.sh'] }

}


