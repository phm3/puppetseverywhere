#sudo groupadd nginx

cd /var
sudo mkdir www
cd /var/www
sudo mkdir html
sudo chown -R vagrant:www-data /var/www/html
find /var/www/html -type f -exec chmod 0660 {} \;
sudo find /var/www/html -type d -exec chmod 2770 {} \;
