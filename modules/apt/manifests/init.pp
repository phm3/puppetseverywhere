class apt {

package { 'apt': ensure =>present }

  exec { "apt-update":
    command => "/usr/bin/apt-get update"
  }
  
   exec { 'apt-get update':
    command => 'apt-get update',
  }

  # Ensure apt is setup before running apt-get update
 # Apt::Key <| |> -> Exec["apt-update"]
 # Apt::Source <| |> -> Exec["apt-update"]

  # Ensure apt-get update has been run before installing any packages
  Exec["apt-update"] -> Package <| |>
}
