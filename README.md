# VAGRANT
This HOWTO is based on my own experience with Vagrant and Puppet software.  

### Install Virtualbox

``# install virtualbox``

Change path globally:

	 ~/.VirtualBox/VirtualBox.xml
	<SystemProperties defaultMachineFolder="/path/to/VirtualBox/VMs" defaultHardDiskFormat="VDI" VRDEAuthLibrary="VBoxAuth" webServiceAuthLibrary="VBoxAuth" LogHistoryCount="3"/>


### Install Vagrant

``apt-get install vagrant``
or use the installer provided by the vendor (the same thing with Puppet)

### Initiate Vagrant machine

For instance: hashicorp/precise64 (the newest version of Vagrant should support for the vagrant cloud, the time Readme.md was created version > 1.5)

``vagrant init hashicorp/precise64``

Search boxes individually*:*

* <https://atlas.hashicorp.com/boxes/search>
* <http://www.vagrantbox.es/>


The name of the machine can be changed in the Vagrantfile: 

	config.vm.define :YOURNAMEHERE do |t|
	  end

Additionally the download url can be edited:

``config.vm.box_url = "http://domain.com/path/to/above.box"``

### Starting the box

``vagrant up``

This command will get the machine to a working state, to provision use Chef or Puppet.
If puppet...

### Install Puppet

Latests release:

	wget https://apt.puppetlabs.com/puppetlabs-release-precise.deb
	sudo dpkg -i puppetlabs-release-precise.deb
	sudo apt-get update


### Create directory:

 ``mkdir -p puppet/{manifests,modules}``

### Provision:

Define settings per modules and include in Vagrantfile the relative path

 ``config.vm.provision :puppet do |puppet|``
``   puppet.manifests_path = "/.../somewhere/puppet/manifests"``
``     puppet.manifest_file  = "default.pp"``
``   end``

**How to return of module path:**

``$ puppet config print modulepath``

Example custom module list:

	puppet module list
	├── Slashbunny-phpfpm (v0.0.7)
	├── example42-apache (v2.1.9)
	├── example42-mysql (v2.1.5)
	├── example42-nginx (v2.3.1)
	├── example42-php (v2.0.20)
	├── example42-puppi (v2.1.11)
	├── nanliu-staging (v1.0.3)
	├── puppet-kafka (v1.0.1)
	├── puppetlabs-apt (v2.1.0)
	├── puppetlabs-concat (v1.2.3)
	├── puppetlabs-git (v0.4.0)
	└── puppetlabs-stdlib (v4.6.0)




 

